var url = "http://iwillgo1-backend.upeu.pe/api/cpanel/public/"
var host = "iwillgo1.upeu.pe"
var url_register = "http://iwillgo1-backend.upeu.pe/api/inscripcion/register/"

Vue.component('video-item', {
    props: ['item'],
    template: `
    <iframe width="500" height="315" :src="item"> </iframe>
    `,
  })


Vue.component('menu-item', {
    props: ['item'],
    template: '<a :href="item.url" class="navListItem menu"> {{item.value}} </a>'
  })


Vue.component('welcome-item', {
    props: ['item'],
    template: `
    <p class="textTour">{{item.description}} <br><br> {{item.biography}}</p>
`,
})

Vue.component('hotel-item', {
    props: ['item'],
    template: `
    <div class="col-md-4 col-sm-6 col-xs-12 wow animated fadeInRight">
    <div class="welcome-block">
          <div class="message-body">
            <h3>{{item.name}}</h3>
                <p v-html="item.description"> </p>
                <p><b>CAMAS</b> - Apertura</p>
                <p><b>20:00</b> - Plenaria: Artur Stele - “El quinto centenario de la Reforma Protestante y la Iglesia Adventista del Séptimo Día”</p>
                <p><b>20:00</b> - Plenaria: Artur Stele - “El quinto centenario de la Reforma Protestante y la Iglesia Adventista del Séptimo Día”</p>
          </div>
    </div>
</div>
    `,
})

var vm = new Vue({
    el: '#app',
    data:{
        event:{},
        theme:{},
        menu:[
            {id:1, url:"#tours", value:'Tours'},
            {id:2, url:"#tours", value:'Hoteles'},
            {id:3, url:"#ponentes", value:'Ponentes'},
            {id:4, url:"#programacion", value:' Programación'},
            {id:4, url:"#testimonios", value:'Testimonios'},
            {id:4, url:"#inscripcion", value:'Inscripcion'}
        ],
        sliders:[],
        section_hotels:{},
        user:{},
        welcome:{},
        tours:[],
        tour_1:{},
        tour_2:{},
        tour_3:{},
        tour_4:{},
    },
    mounted: function(){

    },
    methods: {
        save: function (event) {
            console.log(" Guardando");
            this.user.host = host
            this.$http.post(url_register,this.user)
              .then((response) => {
               console.log("dd");
              })
        },

        getEvent:function(code){
            fetch(`${url}/event/?code=asdasdsd`).
            then(function(response){
                return response.json();
            }).then(json => {
                this.event = json[0];
                this.getTheme(json[0]);
            })
        },
        getTheme: function (event) {
            fetch(url+'event/'+event.id+'/themes').
            then(function(response){
                return response.json();
            }).then(json => {
                this.theme = json[0];
                this.getWelcome(this.theme.id);
                this.getHotels(this.theme.id);
                this.getTours(this.theme.id);
            })
        },
        getWelcome: function (id_theme) {
            fetch(url+'theme/'+id_theme+'/sections/?code=bienvenida').
            then(function(response){
                return response.json();
            }).then(json => {
                this.welcome = json[0].sectionitems[0];
            })
        },
        getTours: function (id_theme) {
            fetch(url+'theme/'+id_theme+'/sections/?code=tours').
            then(function(response){
                return response.json();
            }).then(json => {
                this.tours = json[0].sectionitems;
                this.tour_1 = this.tours[0];
                this.tour_2 = this.tours[1];
                this.tour_3 = this.tours[2];
                this.tour_4 = this.tours[3];
            })
        },
        getSlider: function (id_theme) {
            fetch(url+'theme/'+id_theme+'/sections/?code=slider').
            then(function(response){
                return response.json();
            }).then(json => {
                this.sliders = json[0].sectionitems;
            })
        },
        getHotels: function (id_theme) {
            fetch(url+'theme/'+id_theme+'/sections/?code=hoteles').
            then(function(response){
                return response.json();
            }).then(json => {
                console.log(" Hoteles ");
                console.log(json[0])
                this.section_hotels = json[0];
            })
        },

    },
    computed: {
        getTheme: function (event) {
            console.log( "EVENT"+event);
        }
      }
})
