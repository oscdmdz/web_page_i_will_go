const BASE_API_URL = "http://iwillgo1-backend.upeu.pe/api/cpanel/public";
$( document ).ready(function() {
  AOS.init();
  $('#clock').countdown('2018/09/06 09:00:00', function(event) {
    $(this).html(event.strftime('%D : %H : %M : %S'));
  });
  let test = `asdasd`;
  $(document).on('click', '.openVideo', function (event) {
    event.preventDefault();
    $('#modalIframe').iziModal('open')
  });
  $("#modalIframe").iziModal({
    iframe: true,
    iframeHeight: 600,
    iframeURL: "https://www.youtube.com/embed/kcihcYEOeic?autoplay=1",
    width: 800
  });
  let uluru = {lat: -15.513650, lng: -70.175935};
  let map = new google.maps.Map(document.getElementById('map'), {
    zoom: 14,
    center: uluru
  });
  let marker = new google.maps.Marker({
    position: uluru,
    map: map
  });
  $(document).on('click', '.menu[href^="#"]', function (event) {
    event.preventDefault();
    $('html, body').animate({
        scrollTop: $($.attr(this, 'href')).offset().top
    }, 500);
  });
  $('.tabs .tabLinks a').on('click', function(e)  {
    var currentAttrValue = $(this).attr('href');
    $('.tabs ' + currentAttrValue).show().siblings().hide();
    $(this).parent('li').addClass('active').siblings().removeClass('active');
    e.preventDefault();
  });
  $('.testimonies').slick({
    infinite: true,
    speed: 400,
    autoplay: true,
    autoplaySpeed: 4000,
  });
  let get_event = `${BASE_API_URL}/event/?code=2018-1`;
  $.ajax({
    url: get_event,
    autoplay: true,
    autoplaySpeed: 4000,
    type: "GET"
  })
  .done(function(data){
    let event_id = data[0].id;
    getThemes(event_id)
  })
  .fail(function(err){
    console.log(err);
  })

 
  $('.fade').slick({
  dots: false,
  infinite: true,
  speed: 500,
  fade: true,
  cssEase: 'linear',
  autoplay: true,
  autoplaySpeed: 4000,
});
});
let iconArrowAcc = document.getElementsByClassName('IconAcordeon');
let accTwo = document.getElementsByClassName("acordeon-egTwo");
for (let i = 0; i <  accTwo.length; i++) {
  accTwo[i].addEventListener("click", function(e){
    acordeonStartTwo(this,"transformArrowAcc")
  })
}
function acordeonStartTwo(e,css) {
  let _this = e
  for (let i = 0; i <accTwo.length; i++) {
    if (_this != accTwo[i]) {
      let items = accTwo[i].nextElementSibling;
      items.style.maxHeight = null;
      let arrowAcc = accTwo[i].firstElementChild;
      arrowAcc.classList.remove(css);
    }
    else {
      let item_selected = _this.nextElementSibling;
      let arrowAcc = _this.firstElementChild;
      arrowAcc.classList.add(css)
      if (item_selected.style.maxHeight != ""){
        item_selected.style = "";
        arrowAcc.classList.remove(css)
      } else {
        item_selected.style.maxHeight = item_selected.scrollHeight + "px";
      }
    }
  }
};
let modal = document.getElementById('myModal');
let videoTour = document.getElementsByClassName("videoTour");
let tourContentOne =document.getElementById('tourContentOne');
let tourContentTwo =document.getElementById('tourContentTwo');
let tourContentThere =document.getElementById('tourContentThere');
let tourContentFor =document.getElementById('tourContentFor');
let contentInscribete =document.getElementById('contentInscribete');
let img_tour1  = document.getElementById('tour1');
let img_tour2 = document.getElementById('tour2');
let img_tour3 = document.getElementById('tour3');
let img_tour4 = document.getElementById('tour4');
let inscribete =document.getElementById('inscribete');
if (document.getElementById("inscribete")) {
  // img_tour1.addEventListener("click",tour1);
  // img_tour2.addEventListener("click",tour2);
  // img_tour3.addEventListener("click",tour3);
  // img_tour4.addEventListener("click",tour4);
  inscribete.addEventListener("click",inscribeteSee);
}
function tourModal(id) {
  var description = $('#'+id).attr("data-description");
  var name = $('#'+id).attr("data-name");
  var url = $('#'+id).attr("data-url");

  let modal_div = $('#modal-tours');
  let modal_html = `
  <div class="text-center" style="color:#fff">
  <h1>${name}</h1>
  <iframe width="420" height="315"
  src="${url}"">
  </iframe> 
</div>
<div class="" >
  ${description}
</div>
</div>
  `
  modal_div.empty();
  modal_div.append(modal_html);
    modal.style.display = "block";
}


function inscribeteSee() {
    modal.style.display = "block";
    contentInscribete.style.display ="block";
}
let span = document.getElementsByClassName("close")[0];
span.onclick = function() {
    modal.style.display = "none";
    tourContentOne.style.display ="none";
    tourContentTwo.style.display ="none";
    tourContentThere.style.display ="none";
    tourContentFor.style.display ="none";
}
let cerrar = document.getElementsByClassName("cerrar")[0];
cerrar.onclick = function() {
    modal.style.display = "none";
    tourContentTwo.style.display ="none";
    tourContentThere.style.display ="none";
    tourContentFor.style.display ="none";
}
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
        tourContentOne.style.display ="none";
        tourContentTwo.style.display ="none";
        tourContentThere.style.display ="none";
        tourContentFor.style.display ="none";
    }
}
function getThemes(event_id){
  let url_themes = `${BASE_API_URL}/event/${event_id}/themes`;
  $.ajax({
    url: url_themes,
    type: "GET",
  })
  .done(function(data){
    let theme_id = data[0].id;
    getTours(theme_id);
    getSpeaker(theme_id);
    getHotels(theme_id);
    getPaquetes(theme_id);
  })
  .fail(function(err){
    console.log(err);
  })
}
function getTours(theme_id){
  let url_tours = `${BASE_API_URL}/theme/${theme_id}/sections/?code=tours`;
  $.ajax({
    url: url_tours,
    type: "GET",
  })
  .done(function(data){
    let containerTour = $('#contentTours');
    let tours = data[0].sectionitems
    for (var i in tours) {
      let tour = tours[i]
      let template_tour = `<div class="pictureTour" id="tour1" >
        <img src="${tour.image}" class="imageTour" alt="">
        <div class="contentItemImgText" data-description="${tour.description}" data-url="${tour.url}" data-name="${tour.name}" id="${tour.id}" onclick="tourModal('${tour.id}')">
          <p class="itemImgText">
            ${tour.name}
          </p>
        </div>
      </div>`
      containerTour.append(template_tour);
    }
    executeScrollForTour();
  })
}
function executeScrollForTour(){
  $('.contentTours').slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 3,
    autoplay: true,
    autoplaySpeed: 4000,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });
}


function getSpeaker(theme_id){
  let url_hotels = `${BASE_API_URL}/theme/${theme_id}/sections/?code=ponentes`;
  $.ajax({
    url: url_hotels,
    type: "GET",
  })
  .done(function(data){
    let containerSpeaker = $('#contentSpeaker');
    let speakers = data[0].sectionitems
    for (var i in speakers) {
      let speaker = speakers[i]
      let template_speakers = `
      <div class="centerCarrousel">
      <div class="contentSpeaker" >
        <img src="${speaker.background_image}" alt="" width="100%" height="283px">
        <p class="nameSpeaker">${speaker.name}</p>
        <p class="textSpeaker">${speaker.description}</p>
        <p class="textSpeaker">Nuevo Tiempo</p>
        <div class="iconsSocialsSpeaker">
          <a  href="#" class="iconsFacebookSpeaker">
            <i class="fab fa-facebook-f"></i>
          </>
          <a  href="#" class="iconsTwitterSpeaker">
            <i class="fab fa-twitter"></i>
          </a>
          <a  href="#" class="iconsGoogleSpeaker">
            <i class="fab fa-google-plus-g"></i>
          </a>
        </div>
        <div class="biography">
          <p class="biographyText"><strong>Biografía:</strong>
          ${speaker.biography}
          </p>
        </div>
      </div>

    </div>
      `
      containerSpeaker.append(template_speakers);
    }
    executeScrollForSpeaker();
  })
}

function executeScrollForSpeaker(){
  $('.Speaker').slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 3,
    autoplay: true,
    autoplaySpeed: 4000,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          infinite: true,
          dots: false
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          dots: false

        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          dots: false

        }
      }
    ]
  });
}


function getPaquetes(theme_id){
  let url_paquetes = `${BASE_API_URL}/theme/${theme_id}/sections/?code=inscripcion_paquetes`;
  $.ajax({
    url: url_paquetes,
    type: "GET",
  })
  .done(function(data){
    console.log("Paquetes");
    console.log(data);
    let contentPaquetes = $('#contentPaquetes');
    let paquetes = data[0].sectionitems
    for (var i in paquetes) {
      let paquete = paquetes[i]
      let template_paquete = `
    <div class="AccSquareContent">
    <div class="acordeon-egTwo  AccSquareColor"><i class="fa fa-arrow-circle-down AccSquareIcon " aria-hidden="true"></i>
      <p class="AccSquareTitle">${paquete.name}</p>
    </div>
    <div class="contentAcordeon ">
      <div class="tableOverflow">
        <table class="tableAcc AccSquareOpacityColor">
        <div class="contentPlan">
          <div>
          ${paquete.description}
          </div>
        </div>
      </div>
    </div>
  </div>
  
      `
      contentPaquetes.append(template_paquete);
    }
    executeScrollForPaquetes();
  })
  }
  
  function executeScrollForPaquetes(){
  let iconArrowAcc = document.getElementsByClassName('IconAcordeon');
  
  let accTwo = document.getElementsByClassName("acordeon-egTwo");
  
  for (let i = 0; i <  accTwo.length; i++) {
    accTwo[i].addEventListener("click", function(e){
      acordeonStartTwo(this,"transformArrowAcc")
    })
  }
  
  function acordeonStartTwo(e,css) {
    let _this = e
    for (let i = 0; i <accTwo.length; i++) {
      if (_this != accTwo[i]) {
        let items = accTwo[i].nextElementSibling;
        items.style.maxHeight = null;
        let arrowAcc = accTwo[i].firstElementChild;
        arrowAcc.classList.remove(css);
      }
      else {
        let item_selected = _this.nextElementSibling;
        let arrowAcc = _this.firstElementChild;
        arrowAcc.classList.add(css)
        if (item_selected.style.maxHeight != ""){
          item_selected.style = "";
          arrowAcc.classList.remove(css)
        } else {
          item_selected.style.maxHeight = item_selected.scrollHeight + "px";
        }
      }
    }
  }};



  function getHotels(theme_id){
    let url_hotels = `${BASE_API_URL}/theme/${theme_id}/sections/?code=hoteles`;
    $.ajax({
      url: url_hotels,
      type: "GET",
    })
    .done(function(data){
      let containerHotel = $('#contentHotels');
      let hotels = data[0].sectionitems
      for (var i in hotels) {
        let hotel = hotels[i]
        let template_hotel = `
        <div class="pictureTour" >
        <img src="${hotel.image}" class="imageTour" alt="">
        <div class="contentItemImgTextHotel">
          <p class="itemImgTextHotel">${hotel.name}</p>
        </div>
        <div class="hotelDescription">
          <p class="hotelDescriptionText"> <strong>${hotel.name}</strong> </p>
          <p class="hotelDescriptionText"> <strong>Descripción:</strong> ${hotel.description}</p>
          <p class="hotelDescriptionText"><strong>Telefono:</strong> ${hotel.cell_phone}</p>
          <p class="hotelDescriptionText"><strong>Correo:</strong>${hotel.google_link}</p>
          <p class="hotelDescriptionText"><strong>Costo:</strong> S/ ${hotel.cost}</p>
        </div>
        `
        containerHotel.append(template_hotel);
      }
      executeScrollForHotels();
    })
    }
    
    function executeScrollForHotels(){
     
  $('.contentHotels').slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 3,
    autoplay: true,
    autoplaySpeed: 4000,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });
    }
    