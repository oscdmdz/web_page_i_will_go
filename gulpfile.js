var gulp = require('gulp');
var sass = require('gulp-ruby-sass');
var minifyCSS = require('gulp-csso');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var babel = require('gulp-babel');

gulp.task('default', ['watch']);

gulp.task('watch', function() {
  gulp.watch('web/sass/*.scss', ['sass']);
  gulp.watch('web/js/*.js', ['js']);
});

gulp.task('sass', () =>
    sass('web/sass/style.scss')
      .on('error', sass.logError)
      .pipe(minifyCSS())
      .pipe(gulp.dest('build/css'))
);

gulp.task('js', function(){
  return gulp.src('web/js/*.js')
    .pipe(sourcemaps.init())
    .pipe(babel({
      presets: ['env']
    }))
    .pipe(concat('app.min.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('build/js'))
});
